class User < ActiveRecord::Base
    belongs_to :referrer, :class_name => "User", :foreign_key => "referrer_id"
    has_many :referrals, :class_name => "User", :foreign_key => "referrer_id"

    validates :email, :uniqueness => true, :format => { :with => /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/i, :message => "Invalid email format." }
    validates :referral_code, :uniqueness => true

    before_create :create_referral_code
    after_create :send_welcome_email
    after_create :send_referral_alert, :if => lambda {|u| u.referrer }

    REFERRAL_STEPS = [
        {
            'count' => 5,
            "html" => "15 % off TOLC for life",
            "class" => "two",
        },
        {
            'count' => 10,
            "html" => "50ml TOLC Lube",
            "class" => "three",
        },
        {
            'count' => 15,
            "html" => "100ml TOLC lube",
            "class" => "four",
        },
        {
            'count' => 20,
            "html" => "100ml TOLC lube<br>+ Mystery Sex Toy",
            "class" => "five",
        }
    ]

    private

    def create_referral_code
        referral_code = SecureRandom.hex(5)
        @collision = User.find_by_referral_code(referral_code)

        while !@collision.nil?
            referral_code = SecureRandom.hex(5)
            @collision = User.find_by_referral_code(referral_code)
        end

        self.referral_code = referral_code
    end

    def send_welcome_email
        mail = Notifier.signup_email(self)
        mail.deliver_now
    end

    def send_referral_alert
        mail = Notifier.referral_alert(self.referrer)
        mail.deliver_now
    end
end
