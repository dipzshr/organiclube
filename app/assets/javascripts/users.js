$(document).ready(function(){
	if ($(window).width() > 635) {
		var lengthTwo = $('.circle').offset().left + 30;
		var lengthThree = $('.circle:eq(1)').offset().left + 30;
		var lengthFour = $('.circle:eq(2)').offset().left + 30;
		var setProgress = $('.circle:eq(3)').offset().left - 50;
		var image = '<div class="droplet-desktop">';
		var width = $(window).width();
		var margins = (width - setProgress) / 2;

		$('.progress').css('width', setProgress);
		$('.page-content').css('margin-left', margins);
		$('.page-content').css('margin-right', margins);

		if ($('#check').hasClass('prize-two')) {
			$('.bar').css('width', lengthTwo);
			$('.bar').append(image);
		}
		else if ($('#check').hasClass('prize-three')) {
			$('.bar').css('width', lengthThree);
			$('.bar').append(image);
		}
		else if ($('#check').hasClass('prize-four')) {
			$('.bar').css('width', lengthFour);
			$('.bar').append(image);
		}
		else if ($('#check').hasClass('prize-five')) {
			$('.bar').css('width', setProgress);
			$('.bar').append(image);
		}
		else {
			$('.bar').css('width', 150);
			$('.bar').append(image);
		}
	} else if ($(window).width() <= 635) {
		var image = '<div class="droplet-mobile">';

		$('.bar').append(image);

		if ($('#check').hasClass('prize-two')) {
			$('.droplet-mobile').css('margin-top', 85);
		}
		else if ($('#check').hasClass('prize-three')) {
			$('.droplet-mobile').css('margin-top', 185);
		}
		else if ($('#check').hasClass('prize-four')) {
			$('.droplet-mobile').css('margin-top', 295);
		}
		else if ($('#check').hasClass('prize-five')) {
			$('.droplet-mobile').css('margin-top', 435);
		}
	}

		$(window).on('resize', function(){
			if ($(window).width() > 635) {
				var lengthTwo = $('.circle').offset().left + 30;
				var lengthThree = $('.circle:eq(1)').offset().left + 30;
				var lengthFour = $('.circle:eq(2)').offset().left + 30;
				var setProgress = $('.circle:eq(3)').offset().left - 50;
				var image = '<div class="droplet-desktop">';
				var width = $(window).width();
				var margins = (width - setProgress) / 2;

				$('.progress').css('width', setProgress);
				$('.page-content').css('margin-left', margins);
				$('.page-content').css('margin-right', margins);
				
				if ($('#check').hasClass('prize-two')) {
					$('.bar').css('width', lengthTwo);
					$('.droplet-mobile').replaceWith(image);
				}
				else if ($('#check').hasClass('prize-three')) {
					$('.bar').css('width', lengthThree);
					$('.droplet-mobile').replaceWith(image);
				}
				else if ($('#check').hasClass('prize-four')) {
					$('.bar').css('width', lengthFour);
					$('.droplet-mobile').replaceWith(image);
				}
				else if ($('#check').hasClass('prize-five')) {
					$('.bar').css('width', setProgress);
					$('.droplet-mobile').replaceWith(image);
				}
				else {
					$('.bar').css('width', 150);
					$('.droplet-mobile').replaceWith(image);
				}
			}
			else {
				var image = '<div class="droplet-mobile">';

				$('.bar').css('width', 4);
				$('.page-content').css('margin', 0);
				$('.droplet-desktop').replaceWith(image);

				if ($('#check').hasClass('prize-two')) {
					$('.droplet-mobile').css('margin-top', 85);
				}
				else if ($('#check').hasClass('prize-three')) {
					$('.droplet-mobile').css('margin-top', 185);
				}
				else if ($('#check').hasClass('prize-four')) {
					$('.droplet-mobile').css('margin-top', 295);
				}
				else if ($('#check').hasClass('prize-five')) {
					$('.droplet-mobile').css('margin-top', 435);
				}
			}
		});
});