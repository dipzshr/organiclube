class Notifier < ApplicationMailer
  default from: 'TOLC@organiclube.co',
          return_path: 'system@example.com'

  def signup_email(user)
    @user = user

    mail(:to => user.email,
      :subject => "Thanks for signing up!")
  end

  def referral_alert(user)
    @user = user

    mail(:to => user.email,
      :subject => 'Your friend has joined TOLC')
  end
end
