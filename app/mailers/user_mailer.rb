class UserMailer < ActionMailer::Base
    default from: "Suppot <support@organiclube.com>"

    def signup_email(user)
        @user = user
        @twitter_message = "Slide on in. Excited for #organiclube to launch."

        mail(:to => user.email, :subject => "Thanks for signing up!")
    end
end
